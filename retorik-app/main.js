const {app, BrowserWindow } = require('electron')

let win;

function createWindow() {
    win = new BrowserWindow({
        width:1320,
        height:1080,
        icon:`src/assets/LogoRetorik-violet.png`
    })

    win.removeMenu()
    win.maximize();
    win.setMenu(null);
    
    win.loadURL(`file://${__dirname}/dist/angular-electron/index.html`)

    win.on('closed', function() {
        win = null
    })
}

app.on('ready', createWindow)

app.on('window-all-closed', function() {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function() {
    if (win === null) {
        createWindow()
    }
})