import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor( private _router: Router) { }

  /*navigateToUsers(){

    this._router.navigate(['/users']);
  }

  navigateToMess(){

    this._router.navigate(['/messaging']);
  }*/

  randomUserColor() {
    let num = Math.floor(Math.random() * 4);
    let elem = document.getElementById("icon");
    
    if (elem !== null){
      elem.classList.add(`user-type-${num}`)
    }

  }

  ngOnInit(): void {
    this.randomUserColor()
  }

}
