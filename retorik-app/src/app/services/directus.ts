import { Time } from '@angular/common';
import { JsonpClientBackend } from '@angular/common/http';
import { Directus, ID } from '@directus/sdk';
import { Timestamp } from 'rxjs';
import { environment } from '../../environments/environment';

  const address = 'http://51.254.33.215:8055'

  export default {
  
    async userLogin(email:string, password:string) {

      const request = `?filter={"email":{"_eq": "${email}"},"password":{"_eq": "${password}"}}`
      const url = new URL(`${address}/items/users${request}`);

      return await fetch( url.toString());
    },

    async getTable(table:string) {
      const url = new URL('http://51.254.33.215:8055/items/'+table);
      //url.search = new URLSearchParams(query).toString();
      return await fetch( url.toString(), {} );
    },

    async getTableFilter(table:string, filter:any) {
        const url = new URL('http://51.254.33.215:8055/items/'+table+"?filter="+filter);
        //url.search = new URLSearchParams(query).toString();

        return await fetch( url.toString(), {} );
      },

    async addItems(table:string, request:any) {
    
        const url = new URL('http://51.254.33.215:8055/items/'+table);
        return await fetch(url.toString(), 
        {
          headers:{'Accept': 'application/json',
		  'Content-Type': 'application/json'},
          method: 'POST',
          body: JSON.stringify(request),
        })
		
    },

	async getTableFieldFilter(table:string, field:string,filter:string) {
        const url = new URL('http://51.254.33.215:8055/items/'+table+"?fields="+field+"&filter="+filter);

        return await fetch( url.toString(), {} );
      },

	async getAgregateItems(table:string, aggregate:string, groupby:string){
		const url = new URL('http://51.254.33.215:8055/items/'+table+"?aggregate"+aggregate+"&groupBy="+groupby);
        const headers = {
        "Authorization" : "Bearer 2Q8I2aXd9fQzrknTz8yoXovZ4SKkjUE9",
        };
        return await fetch( url.toString(), {} );
	},

  async getWikipedia(mot:string){
    const url = new URL('https://fr.wikipedia.org/w/api.php?action=opensearch&search='+mot+'&origin=*')
    const headers = {"Access-Control-Allow-Origin": "*" }
    return await fetch( url.toString(), {method:"GET", headers} );
  }


	

    

  }
  

