import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  createdAt = formatDate(new Date(), 'dd/MM/yyyy hh:mm:ss a', 'en');
  informations = 
  [
    {
      
      email: 'test@gmail.com',
      mdp: 'test1',
      nom: 'test1N',
      prenom:'test1P',
    }
  ]

  usersVoc =
  [
    {
      nom:'Bernard',
      img:'assets/LogoRetorik-violet.png',
      talking:false,
    },
    {
      nom:'Arnaud',
      img:'assets/LogoRetorik-violet.png',
      talking:false,
    },
    {
      nom:'Costentin',
      img:'assets/LogoRetorik-violet.png',
      talking:false,
    },
    {
      nom:'Podre',
      img:'assets/LogoRetorik-violet.png',
      talking:false,
    }

  ]

  channelsVoc = [
    {
      name:"Débat Oral 1",
      link:["Bienvenue dans le débat Oral 1 ici seront les liens"],
      messCreatedAt: [this.createdAt],
      createdAt: this.createdAt
    },
    {
      name:"Débat Oral 2",
      messages:["Bienvenue dans le débat Oral 1 ici seront les liens"],
      messCreatedAt: [this.createdAt],
      createdAt: this.createdAt
    }
  ]
  

  channels = [
    {
      name:"Général",
      messages:["Mic","Test","Ok",],
      messCreatedAt: [this.createdAt,this.createdAt,this.createdAt],
      createdAt: this.createdAt
    },
    {
      name:"Memes",
      messages:["Oui","Non","Ok",],
      messCreatedAt: [this.createdAt,this.createdAt,this.createdAt],
      createdAt: this.createdAt
    }
  ]

  currentChannel ={
    name:"Filling",
    messages:["Filling","Filling"],
    messCreatedAt: [this.createdAt,this.createdAt],
    createdAt: this.createdAt
  }

  StockageName : string = "";

  
  setUser(_email:string, _mdp:string, _nom:string, _prenom:string):void{
      
    this.informations.push({
      email : _email,
      mdp : _mdp,
      nom : _nom,
      prenom: _prenom
    })

  }

 setChannel(name:string, messages:string[],messCreatedAt:string[], createdAt:string):void{

  this.channels.push({name,messages,messCreatedAt, createdAt});
  
 }

 selectGroup(test:string):void{

  this.channels.forEach(element => {
    if(element.name==test)
    {
      this.currentChannel = element
    }
  });  

 }

 insertMessage(message:string, createdAt:string):void{
   

  this.channels.forEach(element => {
    if(element.name==this.currentChannel.name)
    {
      element.messages.push(message);
      element.messCreatedAt.push(createdAt);
    }
  });
 }

 insertLink(link:string, createdAt:string):void{
   

  this.channels.forEach(element => {
    if(element.name==this.currentChannel.name)
    {
      element.messages.push(link);
      element.messCreatedAt.push(createdAt);
    }
  });
 }
  


  constructor() { }
}
