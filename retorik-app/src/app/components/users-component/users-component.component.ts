import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UsersService } from '../../services/users.service';

@Component({
  selector: 'app-users-component',
  templateUrl: './users-component.component.html',
  styleUrls: ['./users-component.component.css']
})
export class UsersComponentComponent implements OnInit {

  submitForm(form: NgForm): boolean {
    if( form.value.email === "" || form.value.mdp === "" || form.value.nom ==="" || form.value.prenom === "") {
      throw new Error("Attribute 'a' is required");
   }
   else
   {
    this.usersService.setUser(form.value.email, form.value.mdp, form.value.nom, form.value.prenom );
    return true;
   }
  }

  navigateToUsers(){

    this._router.navigate(['/users']);
  }

  get user(): any {

    return this.usersService.informations;

  }

  users(): any{

    return this.usersService.informations;
  }


  constructor(private usersService: UsersService, private _router: Router) {}

  ngOnInit(): void {
  }

}
