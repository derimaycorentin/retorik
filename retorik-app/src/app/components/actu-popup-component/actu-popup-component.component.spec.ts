import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActuPopupComponentComponent } from './actu-popup-component.component';

describe('ActuPopupComponentComponent', () => {
  let component: ActuPopupComponentComponent;
  let fixture: ComponentFixture<ActuPopupComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActuPopupComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActuPopupComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
