import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-actu-popup-component',
  templateUrl: './actu-popup-component.component.html',
  styleUrls: ['./actu-popup-component.component.css']
})
export class ActuPopupComponentComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

}
