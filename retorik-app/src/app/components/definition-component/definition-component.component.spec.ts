import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinitionComponentComponent } from './definition-component.component';

describe('DefinitionComponentComponent', () => {
  let component: DefinitionComponentComponent;
  let fixture: ComponentFixture<DefinitionComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefinitionComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinitionComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
