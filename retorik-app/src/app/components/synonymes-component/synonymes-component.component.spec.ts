import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SynonymesComponentComponent } from './synonymes-component.component';

describe('SynonymesComponentComponent', () => {
  let component: SynonymesComponentComponent;
  let fixture: ComponentFixture<SynonymesComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SynonymesComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SynonymesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
