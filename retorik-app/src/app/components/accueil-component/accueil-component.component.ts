import { Component, OnInit } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import  directus  from 'src/app/services/directus';

import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');

@Component({
  selector: 'app-accueil-component',
  templateUrl: './accueil-component.component.html',
  styleUrls: ['./accueil-component.component.css']
})
export class AccueilComponentComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    
  }

  //Variable pour appel des actualités
  titre1:string ="Intelligence artificiel, son avancée fulgurante";
  titre2:string ="Matrice Lecko émerge";
  titre3:string ="Le gravimètres quantique révolutionnaire";
  description1:string="L'intelligence artificiel se développe à grande vitesse pour s'intégrer dans le monde de demain";
  description2:string="Matrice lecko, la digital Workplace française se démarque face à Microsoft";
  description3:string="Le gravimètres quantique capable de cartographier les océans en validation";
  image1:string="assets/actuImage/IA.jpg";
  image2:string="assets/actuImage/digitalWorkspace.jpg";
  image3:string="assets/actuImage/graviQuantique.jpg";

  todayDate : string = `${new Date().getUTCDate()}/0${new Date().getUTCMonth()}/${new Date().getUTCFullYear()}`;

}
