import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavigateurComponentComponent } from './navigateur-component.component';

describe('NavigateurComponentComponent', () => {
  let component: NavigateurComponentComponent;
  let fixture: ComponentFixture<NavigateurComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavigateurComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigateurComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
