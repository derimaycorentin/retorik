import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { environment } from '../../../environments/environment';
import  directus  from 'src/app/services/directus';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {

  constructor(private usersService: UsersService, private router: Router, private route: ActivatedRoute) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }  

  async submitForm(form: NgForm) {

    if( form.value.email === "" || form.value.mdp === "") {
      this.router.navigate(['']);
      throw new Error("Veuillez vous authentifier.");
    }

    else
    {
      const response = await directus.userLogin(
        form.value.email,
        form.value.mdp
      )

      const {data, errors} = await response.json()
      

      if (response.ok && data[0] ){
        sessionStorage.setItem('isLog', '1');
        sessionStorage.setItem('id_users', data[0]['id_users'])
        sessionStorage.setItem('id_customers', data[0]['id_customers'])
        this.router.navigate(['/accueil'])
      }
      else {
        new Error("Identifiants incorrects.")
      }
    }
  }

  ngOnInit(): void {
    sessionStorage.clear();
  }

  

}
