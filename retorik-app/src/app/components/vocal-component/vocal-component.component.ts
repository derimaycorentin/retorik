import { Component, Injectable, OnInit } from '@angular/core';
import { Socket } from "ngx-socket-io";
import Peer from 'peerjs';

@Component({
  selector: 'app-vocal-component',
  templateUrl: './vocal-component.component.html',
  styleUrls: ['./vocal-component.component.css']
})

export class VocalComponentComponent implements OnInit {

  private peer: Peer | undefined;
  private nav = <any>navigator;
  private audioGrid = document.getElementById('audioGrid')
  private getUserMedia  = this.nav.getUserMedia || this.nav.webkitGetUserMedia || this.nav.mozGetUserMedia || this.nav.msGetUserMedia;
  
  // private mediaCall: Peer.MediaConnection;
  // private localStreamBs: BehaviorSubject<MediaStream> = new BehaviorSubject(null);
  // public localStream$ = this.localStreamBs.asObservable();
  // private remoteStreamBs: BehaviorSubject<MediaStream> = new BehaviorSubject(null);
  // public remoteStream$ = this.remoteStreamBs.asObservable();

  constructor(private socket: Socket) {  }

  ngOnInit(): void {
    this.launchStream()
  }

  public initPeer(): void {
    if (!this.peer || this.peer.disconnected) {
        const peerJsOptions: Peer.PeerJSOption = {
          config: {
            iceServers: [
            {
              urls: [
                'stun:stun1.l.google.com:19302',
                'stun:stun2.l.google.com:19302',
              ],
            }]
          }
        };
        try {
            let id = sessionStorage.getItem('id_users') || '';
            this.peer = new Peer(id, peerJsOptions);
        } catch (error) {
            console.error(error);
        }
    }
  }

  initVoice() {
    this.socket.emit('join-room', 99, sessionStorage.getItem('id_users'));
    
    this.nav.mediaDevices.getUserMedia({
      audio:true,
      video:false
    }).then( (stream:any) => {
      this.socket.on('user-connected', (userId: any) => {
        this.connectToUser(userId, stream)
      })
    })
  }

  connectToUser(userId:any, stream:any) {
    const call = this.peer?.call(userId, stream)

    let audio = document.createElement('audio')

    call?.on('stream', userStream => {
      console.log(1)
      this.addStream(audio, userStream)
    })

    call?.on('close', () => {
      audio.remove()
    })
  }

  addStream(audio:any, stream:any) {
    audio.srcObject = stream;
    audio.addEventListener('loadedmetadata', () => {
      audio.play()
    })
    if (this.audioGrid) this.audioGrid.append(audio)
  }

  launchStream() {
    this.initPeer()
    if (this.peer) {

      this.peer.on('open', (id) => {
        console.log('My PeerJS ID is:', id);
      });

      this.peer.on('call', function(incoming) {
        incoming.on('stream', function(stream) {
          playStream(stream)
        });
      })
    }
  }

}

function playStream(stream:any) {
  let audio:HTMLAudioElement = new HTMLAudioElement()
  audio.autoplay;
  audio.src = (URL || webkitURL).createObjectURL(stream);
  document.body.append(audio);
}



