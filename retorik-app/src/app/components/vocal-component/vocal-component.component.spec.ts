import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VocalComponentComponent } from './vocal-component.component';

describe('VocalComponentComponent', () => {
  let component: VocalComponentComponent;
  let fixture: ComponentFixture<VocalComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VocalComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VocalComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
