import { DatePipe } from '@angular/common'
import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UsersService } from '../../services/users.service';
import directus from "src/app/services/directus";
import { ID } from '@directus/sdk';
import { Socket } from 'ngx-socket-io';
import { response } from 'express';


let idCustomerUser:any=sessionStorage.getItem('id_customers');
let idUser:any=sessionStorage.getItem('id_users');


@Component({
  selector: 'app-messaging-component',
  templateUrl: './messaging-component.component.html',
  styleUrls: ['./messaging-component.component.css']
})
export class MessagingComponentComponent implements OnInit {

  discussionMessages: any;
  subject: any;
  isScrolled:boolean = false;
  @ViewChild('msg') private msg: ElementRef | undefined;
  @HostListener('scroll', ['$event'])
  onScroll(event: any) {
    if (this.msg?.nativeElement.scrollTop <= this.msg?.nativeElement.scrollHeight*0.8)
    {
      console.log('Michel')
      this.showScrollDiv(true) 
    }
    else
    {
      this.showScrollDiv(false) 
    }
  }
  
  constructor(private router: Router, private route: ActivatedRoute, private usersService:UsersService, private socket: Socket ) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  ngOnInit(): void {
    this.getDiscussionsSubject().then( () => { this.scrollToBottom();});
    this.socket.on("noArgs", async () => {
      await this.getDiscussionsMessages().then( () => { this.scrollToBottom();})
    });
    this.scrollToBottom()
  }

  formatDateAs(date: string) {
    const datePipe = new DatePipe('fr-FR');
    return `${datePipe.transform(date, 'EEEE dd MMMM')} à ${datePipe.transform(date, 'H:mm')}`
  }

  scrollToBottom() {
    if (this.msg)
    {
      try {
        this.msg.nativeElement.scrollTop = this.msg.nativeElement.scrollHeight;
        console.log(this.msg.nativeElement.scrollHeight)
      } catch(err) { console.log(err) }
    }          
  }

  showScrollDiv(val:boolean) {
    this.isScrolled = val;
  }

async getDiscussionsMessages() {
  const response = await directus.getTableFieldFilter("discussions_messages",`id_discussions&fields=id_messages.full_message&fields=id_messages.source_link&fields=id_users.name&fields=id_users.surname&fields=date_created`,`
  {"id_discussions": {"id_customers": {"_eq": "${idCustomerUser}"}}}`);
  const {data,errors}=await response.json();
  if(data[0])
  {
    this.discussionMessages= data;
    // console.log(this.discussionMessages.date_created)
    this.discussionMessages.forEach( (el: any) => {
      if (el.date_created) el.newDate = this.formatDateAs(el.date_created)
    })
  }
}

async getDiscussionsSubject() {
  await this.getDiscussionsMessages();
  
  const response = await directus.getTable(`discussions?fields=id_subjects.subject_name&filter[id_discussions]=${this.discussionMessages[0].id_discussions}`);
  const {data,errors}=await response.json();
  if(data[0])
  {
    this.subject= data[0].id_subjects.subject_name;

  }
}

  async addMessage(form: NgForm) {

    if( form.value.message === "")
    {
      throw new Error("Pas de message.");
    }
    else
    {

      //Messages enregistré dans la BDD
      const response = await directus.addItems(`messages`,{
        source_link: form.value.lien,
        full_message: form.value.message,
      });

      const {data,errors}=await response.json();
      const messageData=data;
      //Discussions message pour effectuer les liaisons
        await directus.addItems(`discussions_messages`,{
          id_users: idUser,
          id_discussions: this.discussionMessages[0].id_discussions,
          id_messages: messageData.id_messages,
      });

      let message:string =form.value.message;
      
      message.split(".").forEach(async sentence => {
        //sentence=message.substring(0, message.indexOf(".")+1);
        //message=message.substring(message.indexOf(".")+1);
        if(sentence != "")
        {
          const response = await directus.addItems(`sentences`,{
            full_sentence: sentence,
          });

          const {data,errors} = await response.json();
          const idSentence = data;
          await directus.addItems(`messages_sentences`,{
            id_sentences: idSentence.id_sentences,
            id_messages: messageData.id_messages,
          });

          sentence.split(" ").forEach(async word => {
          /*if(word.indexOf("."))
          {
            word=word.substring(0, word.indexOf("."))
          }*/
            //sentence2=sentence2.substring(sentence2.indexOf(" ")+1);
          if(word != "")
          {
            const response = await directus.addItems(`words`,{
              word: word,
              id_discussions: this.discussionMessages[0].id_discussions,
              id_messages: messageData.id_messages,
              id_users: sessionStorage.getItem('id_users'),
              id_sentences: idSentence.id_sentences,

              //DEMANDER A CORENTIN 
              //link_definition:
            });          
          
            const {data,errors} = await response.json();
            const wordData = data;
            await directus.addItems(`sentences_words`,{
              id_sentences: idSentence.id_sentences,
              id_words: data.id_words,
            });
          }
        });
      }
      });

      await this.syncMessages().then( ()=> { this.scrollToBottom(); });

    }
  }

  async syncMessages() {
    this.socket.emit("updatedDiscu");
  }

  //POUR SYNONYMES DEFINITION ETC ONCLICK NGIF AVEC BOOL TRUE FALSE POUR AFFICHER (Comme dive app.html)
  async rechercheDefinition(form:NgForm){
    console.log(form.value.texte)
    const response = await directus.getWikipedia(form.value.texte)
    const {data,errors} = await response.json();
    console.log(data)
  }
}





