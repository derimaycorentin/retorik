import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EcritComponentComponent } from './ecrit-component.component';

describe('EcritComponentComponent', () => {
  let component: EcritComponentComponent;
  let fixture: ComponentFixture<EcritComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EcritComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EcritComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
