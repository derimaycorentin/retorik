import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-vocal-users-component',
  templateUrl: './vocal-users-component.component.html',
  styleUrls: ['./vocal-users-component.component.css']
})
export class VocalUsersComponentComponent implements OnInit {

  @Input() image:string="";
  @Input() name:string="";
  @Input() color:string="var(--gray)";
  @Input() icon:string="var(--purple)";
  @Input() talking:boolean=false;

  constructor() { }

  ngOnInit(): void {
  }

}
