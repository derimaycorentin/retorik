import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VocalUsersComponentComponent } from './vocal-users-component.component';

describe('VocalUsersComponentComponent', () => {
  let component: VocalUsersComponentComponent;
  let fixture: ComponentFixture<VocalUsersComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VocalUsersComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VocalUsersComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
