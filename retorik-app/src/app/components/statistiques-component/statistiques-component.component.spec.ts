import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StatistiquesComponentComponent } from './statistiques-component.component';

describe('StatistiquesComponentComponent', () => {
  let component: StatistiquesComponentComponent;
  let fixture: ComponentFixture<StatistiquesComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StatistiquesComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatistiquesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
