import { Component, OnInit, ViewChild  } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { UsersService } from '../../services/users.service';
import { environment } from '../../../environments/environment';
import * as Flexmonster from 'flexmonster';
import { FlexmonsterPivot } from 'ng-flexmonster';
import * as Highcharts from 'highcharts';
import  directus  from 'src/app/services/directus';
import { ArrayType } from '@angular/compiler';


let idCustomerUser:any=sessionStorage.getItem('id_customers')

const Wordcloud = require('highcharts/modules/wordcloud');
Wordcloud(Highcharts);

@Component({
  selector: 'app-statistiques-component',
  templateUrl: './statistiques-component.component.html',
  styleUrls: ['./statistiques-component.component.css']
})

export class StatistiquesComponentComponent implements OnInit {
  discussionMessages: any;
  discussion: any;
  
  FlexNbMots:number=0
  FlexNbMessages:number=0
  FlexAvgMots:number=0

  public activity: any;
  public xData: any;
  public label: any;
  options:any;


  jsonData = [
    {
      
      "Nombre de mots": 5,
      "Cours": "1"
    },
    {
      "Nombre de mots": 14,
      "Cours": "2"
    },
    {
      "Nombre de mots": 7,
      "Cours": "3"
    }
    ,
    {
      "Nombre de mots": 16,
      "Cours": "4"
    }
    ,
    {
      "Nombre de mots": 9,
      "Cours": "5"
    }
]

  jsonData_2 = [
    {
      
      "Nombre de mots": 12,
      "Cours": "1"
    },
    {
      "Nombre de mots": 58,
      "Cours": "2"
    },
    {
      "Nombre de mots": 233,
      "Cours": "3"
    },
    {
      "Nombre de mots": 184,
      "Cours": "4"
    },
    {
      "Nombre de mots": 206,
      "Cours": "5"
    }
]
  jsonData_3 = [
    {
      "Mot" : "de",
      "Occurence" : 6
    },
    {
      "Mot" : "une",
      "Occurence" : 5
    },
    {
      "Mot" : "du",
      "Occurence" : 5
    },
    {
      "Mot" : "faire",
      "Occurence" : 4
    },
    {
      "Mot" : "quantité",
      "Occurence" : 4
    }
  ]


  constructor(private usersService: UsersService, private router: Router, private route: ActivatedRoute) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    var text = "Oui. Une raison pour le succès vaut bien la raison du succès. Je pense qu'il a raison. J'ai envie de te dire Kevin que ton intervention fort pertinente apporte un point de vue innovant. Cependant je reste convaincu du fait que ce n'est pas nécessaire de faire du travail à la maison. Une quantité faible permet d'apprendre. Une grande quantité a cependant plus tendance a répugner et faire reculer l'élève fasse à l'étude. J'ai envie de te dire Kevin que ton intervention fort pertinente apporte un point de vue innovant. Cependant je reste convaincu du fait que ce n'est pas nécessaire de faire du travail à la maison. Une quantité faible permet d'apprendre. Une grande quantité a cependant plus tendance a répugner et faire reculer l'élève fasse à l'étude.";
    var obj = {name:'', weight:0};
    var lines = text.split(/[,\. ]+/g),
      
    data = Highcharts.reduce(lines, function (arr: { name: string; weight: number; }[], word: any) {
        
        obj = Highcharts.find(arr, function (obj: { name: any; }) {
            return obj.name === word;
        })!;
            if (obj) {
            
                obj.weight += 1;
            } else {
                obj = {
                    name: word,
                    weight: 1
                };
                arr.push(obj);
            }
            return arr;
        }, []);
    
        this.options = {
        accessibility: {
            screenReaderSection: {
                beforeChartFormat: '<h5>{chartTitle}</h5>' +
                    '<div>{chartSubtitle}</div>' +
                    '<div>{chartLongdesc}</div>' +
                    '<div>{viewTableButton}</div>'
            }
        },
        series: [{
            type: 'wordcloud',
            data: data,
            name: 'Occurrences',
        }],
        chart: {
          backgroundColor: 'var(--dark-blue)',
        },
        colors: ['var(--purple)', 'var(--light-purple)', '#FFFFFF'],
        
        title: {
            text: ''
        }
    };
  }  

  ngOnInit(): void {
    Highcharts.chart('container', this.options);
  }

  //Requêtes Directus
  async getDiscussionsMessages() {
  

    const response = await directus.getTableFieldFilter("discussions_messages",
    `id_discussions.id_customers&fields=id_messages.full_message&fields=id_messages.source_link`,`
    {"id_discussions": {"id_customers": {"_eq": "${idCustomerUser}"}}}`);
    const {data,errors}= await response.json();

    if(data[0])
    {
      this.discussionMessages = data;
    }

    var jsonData_2 = [
      {
        
        "Nombre de mots": 12,
        "Cours": "1"
      },
      {
        "Nombre de mots": 58,
        "Cours": "2"
      },
      {
        "Nombre de mots": 233,
        "Cours": "3"
      },
      {
        "Nombre de mots": 184,
        "Cours": "4"
      },
      {
        "Nombre de mots": 206,
        "Cours": "5"
      }
  ]

    //this.pivot_2.flexmonster.updateData({ data: jsonData_2 });
  }

  async getCountMotNbDiscussions() {
    const response = await directus.getAgregateItems("words",`[count]=word`,`id_discussions`);
    const {data,errors}=await response.json();
    let nbMot:number=0;
    let nbMessage:number=0;
    let AvgMot:number;
  
    this.discussion= data;
    data.forEach((element: any) => {
  
      nbMot+=Number(element.count.word);
      nbMessage+=1;
  
    });
  
    console.log(this.discussion)
    AvgMot = nbMot/nbMessage

    //Données JSON des graphiques
      this.jsonData = [
        {
          
          "Nombre de mots": nbMot,
          "Cours": "1"
        },
        {
          "Nombre de mots": 14,
          "Cours": "2"
        },
        {
          "Nombre de mots": 7,
          "Cours": "3"
        }
        ,
        {
          "Nombre de mots": 16,
          "Cours": "4"
        }
        ,
        {
          "Nombre de mots": 9,
          "Cours": "5"
        }
    ];

    //this.pivot.flexmonster.updateData({ data: this.jsonData });
    
  }

  async getCountSentenceNbDiscussions() {
    const response = await directus.getTable(`words?aggregate[countDistinct]=id_sentences&groupBy=id_messages&groupBy=id_discussions&sort=id_discussions&filter[id_users][_eq]=${sessionStorage.getItem('id_users')}`);
    const {data,errors}=await response.json();
    let nbMot:number=0; let nbMessage:number=0; let AvgMot:number; let AvgMotDiscussions:any[] = []; let idDiscussion:number; let i:number = 0;
  
    this.discussion= data;
    data.forEach((element: any) => {
      if(element.id_discussions != idDiscussion && idDiscussion != undefined)
      {
        AvgMotDiscussions[i]=AvgMot = nbMot/nbMessage
        i++;
        nbMot=0
        nbMessage=0
      }
      nbMot+=Number(element.countDistinct.id_sentences);
      nbMessage+=1;
      idDiscussion=element.id_discussions
      
    });

      console.log(this.discussion[0]);
  
      AvgMotDiscussions[i]=AvgMot = nbMot/nbMessage

      var jsonData_3 = [
        {
          "Mot" : "de",
          "Occurence" : 6
        },
        {
          "Mot" : "une",
          "Occurence" : 5
        },
        {
          "Mot" : "du",
          "Occurence" : 5
        },
        {
          "Mot" : "faire",
          "Occurence" : 4
        },
        {
          "Mot" : "quantité",
          "Occurence" : 4
        }
      ];
      this.pivot_3.flexmonster.updateData({ data: jsonData_3 });
      //return jsonData_3;
  }

  async getCountMotNbDiscussionsV() {
    const response = await directus.getTable(`words?aggregate[count]=id_words&groupBy=id_messages&groupBy=id_discussions&sort=id_discussions&filter[id_users][_eq]=${sessionStorage.getItem('id_users')}`);
    const {data,errors}=await response.json();
    let nbMot:number=0;
    let nbMessage:number=0;
    let AvgMot:number;
    let AvgMotDiscussions:any[] = []
    let idDiscussion:number
    let i:number = 0;
  
    this.discussion= data
    data.forEach((element: any) => {
      if(element.id_discussions != idDiscussion && idDiscussion != undefined)
      {
        AvgMotDiscussions[i]=AvgMot = nbMot/nbMessage
        i++;
        nbMot=0
        nbMessage=0
      }
      nbMot+=Number(element.count.id_words);
      nbMessage+=1;
      idDiscussion=element.id_discussions
      
    });
  
      AvgMotDiscussions[i]=AvgMot = nbMot/nbMessage
    
  }

  async getCountMotMessages() {
    const response = await directus.getTable(`words?aggregate[count]=id_words&groupBy=id_sentences&groupBy=id_discussions&sort=id_discussions&filter[id_users][_eq]=${sessionStorage.getItem('id_users')}`);
    const {data,errors}=await response.json();
    let nbMot:number=0;
    let nbSentence:number=0;
    let AvgMot:number;
    let AvgMotDiscussions:any[] = []
    let idDiscussion:number
    let i:number = 0;
  
    this.discussion= data
    data.forEach((element: any) => {
      if(element.id_discussions != idDiscussion && idDiscussion != undefined)
      {
        AvgMotDiscussions[i]=AvgMot = nbMot/nbSentence
        i++;
        nbMot=0
        nbSentence=0
      }
      nbMot+=Number(element.count.id_words);
      nbSentence+=1;
      idDiscussion=element.id_discussions
      
    });
  
      AvgMotDiscussions[i]=AvgMot = nbMot/nbSentence
    
  }

  //Les objets "Report" sont là pour configurer les différents graphiques

  @ViewChild('pivot') pivot!: FlexmonsterPivot;
  @ViewChild('pivot_2') pivot_2!: FlexmonsterPivot;
  @ViewChild('pivot_3') pivot_3!: FlexmonsterPivot;

  public report: Object = {

    dataSource: {
      data: this.jsonData
    },
    slice: {
      rows: [{
        uniqueName: "Cours"
      }],
      columns: [{
        uniqueName: "[Measures]"
      }],
      measures: [{
          uniqueName: "Nombre de mots",
          aggregation: "sum"
        }
      ],
    },
    options: {
      "viewType": "charts",
      "configuratorButton": false,
      chart: {
        "multipleMeasures": true,
        "type": "column",
        "showMeasures": false,
        "showFilter": false,
      }
    }
  };

  public report_2: Object = {
    
    dataSource: {
      data: this.jsonData_2
    },
    slice: {
      rows: [{
        uniqueName: "Cours"
      }],
      columns: [{
        uniqueName: "[Measures]"
      }],
      measures: [{
          uniqueName: "Nombre de mots",
          aggregation: "sum"
        }
      ],
    },
    options: {
      "viewType": "charts",
      "configuratorButton": false,
      chart: {
        "multipleMeasures": true,
        "type": "line",
        "showMeasures": false,
        "showFilter": false,
      }
    }
  };

  public report_3: Object = {
    
    dataSource: {

      data: this.jsonData_3
      
    },
    options: {
      "viewType": "charts",
      "configuratorButton": false,
      "chart": {
        "type": "pie",
        "showMeasures": false,
        "showFilter": false,
      }
    }
  };
 
  //Configuration des toolbar des différents graphiques
  customizeToolbar(toolbar: Flexmonster.Toolbar) 
  {
    let tabs = toolbar.getTabs();
    toolbar.getTabs = () => {
      tabs = [];
      tabs.push({
        title: "Graph",
        id: "fm-tab-RetorikCharts",
        icon: toolbar.icons.charts,
        menu: [{
                title: "Diagramme en barres",
                id: "fm-tab-charts-retorikColumn",
                handler: columnHandler,
                args: "column",
                icon: toolbar.icons.charts
              },
              {
                title: "Courbes",
                id: "fm-tab-charts-retorikLine",
                handler: lineHandler,
                args: "line",
                icon: toolbar.icons.charts_line
              },
              {
                title: "Diagramme circulaire",
                id: "fm-tab-charts-retorikPie",
                handler: pieHandler,
                args: "pie",
                icon: toolbar.icons.charts_pie
              },
            ]
      });
      tabs.push({
        title: "Grille",
        id: "fm-tab-Retorikgrid",
        handler: toolbar.gridHandler,
        icon: toolbar.icons.grid
      });
      tabs.push({  
        title: "Export",
        id: "fm-tab-exporter",
        mobile: false,
        rightGroup: true,
        icon: toolbar.icons.export,
        menu: [
          {
            title: "Imprimer",
            id: "fm-tab-export-imprimer",
            handler: toolbar.printHandler,
            icon: toolbar.icons.export_print
          },
          {
            title: "en PDF",
            id: "fm-tab-export-retorikPdf",
            handler: pdfHandler,
            icon: toolbar.icons.export_pdf
          },
          {
            title: "en Image",
            id: "fm-tab-export-retorikImage",
            handler: ImageHandler,
            icon: toolbar.icons.export_image
          },
          {
            title: "sous format Excel",
            id: "fm-tab-export-retorikExcel",
            handler: excelHandler,
            icon: toolbar.icons.export_excel
          },
        ]
      });
      /*tabs.push({
        title: "¨Plein-écran",
        id: "fm-tab-retorikFullscreen",
        handler: toolbar.fullscreenHandler,
        mobile: false,
        icon: toolbar.icons.fullscreen,
        rightGroup: true
      });*/
      return tabs;
    }
    const paramsExport = {
      filename: 'flexmonster',
      header: "##CURRENT-DATE##",
      footer: "<div>##PAGE-NUMBER##</div>",
      pageOrientation: 'landscape'
    };
    var excelHandler = () =>
    { 
      this.pivot.flexmonster.exportTo('excel', paramsExport);
    } 
    var ImageHandler = () =>
    { 
      this.pivot.flexmonster.exportTo('image', paramsExport);
    } 
    var pdfHandler = () =>
    { 
      this.pivot.flexmonster.exportTo('pdf', paramsExport);
    } 
    var columnHandler = () =>
    { 
      this.pivot.flexmonster.showCharts('column');
    } 
    var lineHandler = () =>
    { 
      this.pivot.flexmonster.showCharts('line');
    } 
    var pieHandler = () =>
    { 
      this.pivot.flexmonster.showCharts('pie');
    } 
  }

  customizeToolbar_2(toolbar: Flexmonster.Toolbar) 
  {
    let tabs = toolbar.getTabs();
    toolbar.getTabs = () => {
      tabs = [];
      tabs.push({
        title: "Graph",
        id: "fm-tab-RetorikCharts_2",
        icon: toolbar.icons.charts,
        menu: [{
                title: "Diagramme en barres",
                id: "fm-tab-charts-retorikColumn_2",
                handler: columnHandler,
                args: "column",
                icon: toolbar.icons.charts
              },
              {
                title: "Courbes",
                id: "fm-tab-charts-retorikLine_2",
                handler: lineHandler,
                args: "line",
                icon: toolbar.icons.charts_line
              },
              {
                title: "Diagramme circulaire",
                id: "fm-tab-charts-retorikPie_2",
                handler: pieHandler,
                args: "pie",
                icon: toolbar.icons.charts_pie
              },
            ]
      });
      tabs.push({
        title: "Grille",
        id: "fm-tab-Retorikgrid_2",
        handler: toolbar.gridHandler,
        icon: toolbar.icons.grid
      });
      tabs.push({  
        title: "Export",
        id: "fm-tab-exporter_2",
        mobile: false,
        rightGroup: true,
        icon: toolbar.icons.export,
        menu: [
          {
            title: "Imprimer",
            id: "fm-tab-export-imprimer_2",
            handler: toolbar.printHandler,
            icon: toolbar.icons.export_print
          },
          {
            title: "en PDF",
            id: "fm-tab-export-retorikPdf_2",
            handler: pdfHandler,
            icon: toolbar.icons.export_pdf
          },
          {
            title: "en Image",
            id: "fm-tab-export-retorikImage_2",
            handler: ImageHandler,
            icon: toolbar.icons.export_image
          },
          {
            title: "sous format Excel",
            id: "fm-tab-export-retorikExcel_2",
            handler: excelHandler,
            icon: toolbar.icons.export_excel
          },
        ]
      });
      /*tabs.push({
        title: "¨Plein-écran",
        id: "fm-tab-retorikFullscreen",
        handler: toolbar.fullscreenHandler,
        mobile: false,
        icon: toolbar.icons.fullscreen,
        rightGroup: true
      });*/
      return tabs;
    }
    const paramsExport = {
      filename: 'flexmonster',
      header: "##CURRENT-DATE##",
      footer: "<div>##PAGE-NUMBER##</div>",
      pageOrientation: 'landscape'
    };
    var excelHandler = () =>
    { 
      this.pivot_2.flexmonster.exportTo('excel', paramsExport);
    } 
    var ImageHandler = () =>
    { 
      this.pivot_2.flexmonster.exportTo('image', paramsExport);
    } 
    var pdfHandler = () =>
    { 
      this.pivot_2.flexmonster.exportTo('pdf', paramsExport);
    } 
    var columnHandler = () =>
    { 
      this.pivot_2.flexmonster.showCharts('column');
    } 
    var lineHandler = () =>
    { 
      this.pivot_2.flexmonster.showCharts('line');
    } 
    var pieHandler = () =>
    { 
      this.pivot_2.flexmonster.showCharts('pie');
    } 
  }

  customizeToolbar_3(toolbar: Flexmonster.Toolbar) 
  {
    let tabs = toolbar.getTabs();
    toolbar.getTabs = () => {
      tabs = [];
      tabs.push({
        title: "Graph",
        id: "fm-tab-RetorikCharts_3",
        icon: toolbar.icons.charts,
        menu: [{
                title: "Diagramme en barres",
                id: "fm-tab-charts-retorikColumn_3",
                handler: columnHandler,
                args: "column",
                icon: toolbar.icons.charts
              },
              {
                title: "Courbes",
                id: "fm-tab-charts-retorikLine_3",
                handler: lineHandler,
                args: "line",
                icon: toolbar.icons.charts_line
              },
              {
                title: "Diagramme circulaire",
                id: "fm-tab-charts-retorikPie_3",
                handler: pieHandler,
                args: "pie",
                icon: toolbar.icons.charts_pie
              },
            ]
      });
      tabs.push({
        title: "Grille",
        id: "fm-tab-Retorikgrid_3",
        handler: toolbar.gridHandler,
        icon: toolbar.icons.grid
      });
      tabs.push({  
        title: "Export",
        id: "fm-tab-exporter_3",
        mobile: false,
        rightGroup: true,
        icon: toolbar.icons.export,
        menu: [
          {
            title: "Imprimer",
            id: "fm-tab-export-imprimer_3",
            handler: toolbar.printHandler,
            icon: toolbar.icons.export_print
          },
          {
            title: "en PDF",
            id: "fm-tab-export-retorikPdf_3",
            handler: pdfHandler,
            icon: toolbar.icons.export_pdf
          },
          {
            title: "en Image",
            id: "fm-tab-export-retorikImage_3",
            handler: ImageHandler,
            icon: toolbar.icons.export_image
          },
          {
            title: "sous format Excel",
            id: "fm-tab-export-retorikExcel_3",
            handler: excelHandler,
            icon: toolbar.icons.export_excel
          },
        ]
      });
      /*tabs.push({
        title: "¨Plein-écran",
        id: "fm-tab-retorikFullscreen",
        handler: toolbar.fullscreenHandler,
        mobile: false,
        icon: toolbar.icons.fullscreen,
        rightGroup: true
      });*/
      return tabs;
    }
    const paramsExport = {
      filename: 'flexmonster',
      header: "##CURRENT-DATE##",
      footer: "<div>##PAGE-NUMBER##</div>",
      pageOrientation: 'landscape'
    };
    var excelHandler = () =>
    { 
      this.pivot_3.flexmonster.exportTo('excel', paramsExport);
    } 
    var ImageHandler = () =>
    { 
      this.pivot_3.flexmonster.exportTo('image', paramsExport);
    } 
    var pdfHandler = () =>
    { 
      this.pivot_3.flexmonster.exportTo('pdf', paramsExport);
    } 
    var columnHandler = () =>
    { 
      this.pivot_3.flexmonster.showCharts('column');
    } 
    var lineHandler = () =>
    { 
      this.pivot_3.flexmonster.showCharts('line');
    } 
    var pieHandler = () =>
    { 
      this.pivot_3.flexmonster.showCharts('pie');
    } 
  }
  //La fonction onReady applique la configuration aux différent graphiques
  onReady() {
    this.pivot.flexmonster.setReport(this.report)
    this.getCountMotNbDiscussions()
  }
  onReady_2() {
    this.pivot_2.flexmonster.setReport(this.report_2)
    this.getDiscussionsMessages()
  }
  onReady_3() {
    this.pivot_3.flexmonster.setReport(this.report_3)
    this.getCountSentenceNbDiscussions()
  }
}


