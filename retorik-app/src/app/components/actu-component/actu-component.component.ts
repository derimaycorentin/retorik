import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-actu-component',
  templateUrl: './actu-component.component.html',
  styleUrls: ['./actu-component.component.css']
})
export class ActuComponentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  titre1:string ="Intelligence artificiel, son avancée fulgurante";
  titre2:string ="Matrice Lecko émerge";
  titre3:string ="Le gravimètres quantique révolutionnaire";
  description1:string="L'intelligence artificiel se développe à grande vitesse pour s'intégrer dans le monde de demain";
  description2:string="Matrice lecko, la digital Workplace française se démarque face à Microsoft";
  description3:string="Le gravimètres quantique capable de cartographier les océans en validation";
  image1:string="assets/actuImage/IA.jpg";
  image2:string="assets/actuImage/digitalWorkspace.jpg";
  image3:string="assets/actuImage/graviQuantique.jpg";

  todayDate : string = `${new Date().getUTCDate()}/0${new Date().getUTCMonth()}/${new Date().getUTCFullYear()}`;

}
