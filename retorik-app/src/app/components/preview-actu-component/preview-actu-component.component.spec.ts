import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewActuComponentComponent } from './preview-actu-component.component';

describe('PreviewActuComponentComponent', () => {
  let component: PreviewActuComponentComponent;
  let fixture: ComponentFixture<PreviewActuComponentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreviewActuComponentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewActuComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
