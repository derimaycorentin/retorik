import { Component, OnInit, Input, HostListener } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { ActuPopupComponentComponent } from '../actu-popup-component/actu-popup-component.component';

@Component({
  selector: 'app-preview-actu-component',
  templateUrl: './preview-actu-component.component.html',
  styleUrls: ['./preview-actu-component.component.css']
})
export class PreviewActuComponentComponent implements OnInit {

  @Input() image:string="";
  @Input() titre:string="";
  @Input() description:string=""

  @Input() height:string="13rem"
  @Input() width:string="30rem"

  logo:string= "assets/actuImage/figaro.png"

  @HostListener("click") onClick(){
    this.dialog.open(ActuPopupComponentComponent);
  }

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
    
    
    
  }

}

