import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { NgModule } from '@angular/core';
//import * as Flexmonster from 'flexmonster';
//import { FlexmonsterPivot, FlexmonsterPivotModule } from 'ng-flexmonster';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string = 'angular-electron';
  showHead: boolean = false;

  constructor(private router: Router) {
    // on route change to '/login', set the variable showHead to false
      router.events.forEach((event) => {
        if (event instanceof NavigationStart) {
          if (event['url'] == '/login' || event['url'] == '/') {
            this.showHead = false;
          } else {
            // console.log("NU")
            this.showHead = true;
          }
        }
        
        if (event instanceof NavigationStart) {
          if (event['url'] !== '/login' && event['url'] !== '/') {
            if (sessionStorage.getItem('isLog') === null){
              this.router.navigate(['']);
            }
          }
        }
        
      });
    }

}
