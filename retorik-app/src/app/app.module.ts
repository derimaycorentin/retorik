import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FlexmonsterPivotModule } from 'ng-flexmonster';
import { HighchartsChartModule } from 'highcharts-angular';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponentComponent } from './components/login-component/login-component.component';
import { UsersComponentComponent } from './components/users-component/users-component.component';
import { MessagingComponentComponent } from './components/messaging-component/messaging-component.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { AccueilComponentComponent } from './components/accueil-component/accueil-component.component';
import { PreviewActuComponentComponent } from './components/preview-actu-component/preview-actu-component.component';
import { DefinitionComponentComponent } from './components/definition-component/definition-component.component';
import { SynonymesComponentComponent } from './components/synonymes-component/synonymes-component.component';
import { StatistiquesComponentComponent } from './components/statistiques-component/statistiques-component.component';
import { NavigateurComponentComponent } from './components/navigateur-component/navigateur-component.component';
import { VocalComponentComponent } from './components/vocal-component/vocal-component.component';
import { EcritComponentComponent } from './components/ecrit-component/ecrit-component.component';
import { ActuComponentComponent } from './components/actu-component/actu-component.component';
import { ArchivesComponentComponent } from './components/archives-component/archives-component.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';
import { VocalUsersComponentComponent } from './components/vocal-users-component/vocal-users-component.component';
import { ErrorComponentComponent } from './components/error-component/error-component.component';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { ActuPopupComponentComponent } from './components/actu-popup-component/actu-popup-component.component';

const config: SocketIoConfig = { url: 'http://51.254.33.215:3000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    LoginComponentComponent,
    UsersComponentComponent,
    MessagingComponentComponent,
    HeaderComponent,
    AccueilComponentComponent,
    PreviewActuComponentComponent,
    DefinitionComponentComponent,
    SynonymesComponentComponent,
    StatistiquesComponentComponent,
    NavigateurComponentComponent,
    VocalComponentComponent,
    EcritComponentComponent,
    ActuComponentComponent,
    ArchivesComponentComponent,
    VocalUsersComponentComponent,
    ErrorComponentComponent,
    ActuPopupComponentComponent
  ],
  entryComponents: [
    ActuPopupComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexmonsterPivotModule,
    HighchartsChartModule,
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatTabsModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatDialogModule,
    SocketIoModule.forRoot(config)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
