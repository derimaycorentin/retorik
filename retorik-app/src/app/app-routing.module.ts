import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponentComponent } from './components/login-component/login-component.component';
import { MessagingComponentComponent } from './components/messaging-component/messaging-component.component';
import { UsersComponentComponent } from './components/users-component/users-component.component';
import { AccueilComponentComponent } from './components/accueil-component/accueil-component.component';
import { VocalComponentComponent } from './components/vocal-component/vocal-component.component';
import { StatistiquesComponentComponent } from './components/statistiques-component/statistiques-component.component';
import { ErrorComponentComponent } from './components/error-component/error-component.component';
import { ActuComponentComponent } from './components/actu-component/actu-component.component';

const routes: Routes = [

  {
    path: 'login',
    component: LoginComponentComponent,
  },

  {
    path: 'accueil',
    component: AccueilComponentComponent,
  },

  {
    path: 'actualites',
    component: ActuComponentComponent,
  },

  {
    path: 'users',
    component: UsersComponentComponent,
  },

  {
    path: 'messaging',
    component: MessagingComponentComponent,
  },

  {
    path: 'vocal',
    component: VocalComponentComponent,
  },
  {
    path: 'statistiques',
    component: StatistiquesComponentComponent,
  },
  {
    path: '404',
    component: ErrorComponentComponent,
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
