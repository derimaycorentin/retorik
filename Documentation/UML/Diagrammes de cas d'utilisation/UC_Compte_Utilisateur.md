@startuml
left to right direction

actor Utilisateur as user

package "Créer son compte" as C {
  usecase "Choisir son abonnement (PRO, PERSONNALISATION, GRATUIT)" as UC1
  usecase "Personnaliser son profil" as UC2
  usecase "Sécuriser son compte" as UC3
}

package "Discussion privée (PRO)" as M {
  usecase "Discuter avec un autre membre" as UC6
}

package "Choisir son débat" as D {
  usecase "Oral ou écrit ?" as UC4
  usecase "Degré de difficulté ? Niveau de modération ?" as UC5
}

user --> UC1
user --> UC2
user --> UC3

UC1 --> UC6

C --> D

@enduml