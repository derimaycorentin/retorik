@startuml
left to right direction

actor "Utilisateur" as user
actor "Professionnel" as pro

usecase "Passer une certification RÉTORIK" as UC1
usecase "Devenir modérateur" as UC2
usecase "Postuler pour devenir modérateur" as UC3

user --> UC1
pro --> UC3
UC1 --> UC2
UC3 --> UC2

@enduml