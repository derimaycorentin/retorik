# Les utilisateurs peuvent débattre entre eux sur le channel écrit. Ils peuvent sourcer leurs propos, et doivent voter au début et à la fin du débat, exprimant ainsi leur position sur le débat. Le modérateur peut 
@startuml
left to right direction

actor Utilisateur as user
actor Modérateur as m

package "Channel écrit" as C {
  usecase "Voter pour sa position" as UC1
  usecase "Argumenter sa position" as UC2
  usecase "Sourcer ses propos" as UC3
  usecase "Choisir sa position finale" as UC4 
  usecase "Rechercher définitions et synonymes" as UC8

  usecase "Sanctionner ou avertir" as UC5
  usecase "Taguer un message (biais, faute...)" as UC6
  usecase "Écrire des messages pour recentrer le débat, avertir..." as UC7
}

user --> UC1
user --> UC2
user --> UC3
user --> UC4
user --> UC8
m --> UC5
m --> UC6
m --> UC7

user <|-- m

@enduml
