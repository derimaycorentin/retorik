
@startuml
left to right direction

actor Utilisateur as user
actor Modérateur as m

package "Channel écrit" as C {
  usecase "Voter pour sa position" as UC1
  usecase "Sourcer ses propos" as UC2
  usecase "Choisir sa position finale" as UC3

  usecase "Avertir et sanctionner" as UC4
}

package "Channel vocal" as V {
  usecase "Argumenter sa position" as UC5

  usecase "Avertir et sanctionner" as UC6
  usecase "Recentrer le débat, avertir..." as UC7
}

user --> UC1
user --> UC2
user --> UC3
user --> UC5

m --> UC4
m --> UC6
m --> UC7

user <|-- m

@enduml
