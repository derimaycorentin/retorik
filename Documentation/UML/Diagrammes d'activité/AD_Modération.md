
	
@startuml
start
:Modération;
:Rejoindre le débat à modérer;
:Observer le débat;
if(Faute d'un utilisateur ?) is (Oui) then
    :Tag du message;
    :Sanction ou avertissement;
else
endif
if(Débat plus centrer sur le sujet ?) is (Oui) then
    :Recentrer le débat;
else
endif

end

@enduml