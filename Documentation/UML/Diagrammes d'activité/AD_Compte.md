
	
@startuml
start
:Compte;
if (Compte utilisateur ?) is (Oui) then
    :Se connecter;
else 
    :Créer un compte;
endif
if(Abonné ?) is (Non) then
    if(Veut s'abonner ?) is (Oui) then
    :Choisir un abonnement;
    :Personnaliser son compte;
    else
    :Obtenir la version gratuite;
    endif
else
endif

:Naviguer sur le site;
end

@enduml