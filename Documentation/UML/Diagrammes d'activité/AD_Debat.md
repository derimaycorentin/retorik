
	
@startuml
start
:Débat;
if(Veut participer à un débat ?) is (Oui) then
    :Choisir le type de débat;
    :Rejoindre le débat;
    :Choisir sa position;
    :Argumenter sa position;
    if(Sourcer son message ?) is (Oui) then
        :Ajouter une source;
    else
    endif
    :Envoyer le message;

else
    :Choisir sa position;
    :Observer un débat;
    :Augmenter la visibilité 
            d'un message;
endif
:Choisir sa position finale;
end

@enduml