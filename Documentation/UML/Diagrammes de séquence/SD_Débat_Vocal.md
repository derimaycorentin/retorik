@startuml

Modérateur -> Application: Connexion
Modérateur -> Application: Créer une discussion dans un channel vocal
Application -> Application: Création de la discussion

Utilisateur -> Application: Connexion
Utilisateur -> Application: Rejoindre la discussion sur le channel vocal
group Débat
Utilisateur -> Application: Voter sa position dans le débat sur le channel écrit
Application -> Application: Enregistrement du vote de l'utilisateur
    loop
        
        Modérateur -> Application: Animer le débat en fonction du niveau de la discussion
        Utilisateur -> Application: Argumenter sa position sur le channel vocal
        Utilisateur -> Application: Citer ses sources sur le channel écrit
        
        alt l'Utilisateur enfreint le règlement
            Modérateur -> Application: Sanctionner ou avertir des utilisateurs s'ils enfreignent le règlement de Rétorik
            Application -> Utilisateur: Sanctionner l'utilisateur
        end
    end
Utilisateur -> Application: Voter sa position finale sur le channel écrit
Application -> Application: Enregistrement du vote final de l'utilisateur
end

Modérateur -> Application: Mettre fin à la discussion
Application -> Application: Suppression de la discussion

@enduml
