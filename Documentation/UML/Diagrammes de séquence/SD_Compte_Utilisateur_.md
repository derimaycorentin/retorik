@startuml
Utilisateur -> Application: Créer le compte
Application -> Application: Création du compte utilisateur
Application -> Utilisateur: Demande si l'utilisateur veut s'abonner

alt l'utilisateur veut s'abonner
    Utilisateur -> Application: Choisit son abonnement
    Application -> Application: Attribution des droits utilisateur
else l'utilisateur ne veut pas s'abonner
    Utilisateur -> Application: Choisit la version gratuite
        Application -> Application: Attribution des droits utilisateur
end
alt l'utilisateur est abonné et souhaite personnaliser son compte
    Utilisateur -> Application: Personnaliser le compte
    Application -> Application: Enregistrement des modifications
end
Utilisateur -> Application: Naviguer sur le site
@enduml
