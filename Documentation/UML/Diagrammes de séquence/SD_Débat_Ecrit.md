@startuml

Modérateur -> Application: Connexion
Modérateur -> Application: Créer une discussion dans un channel écrit
Application -> Application: Création de discussion

Utilisateur -> Application: Connexion
Utilisateur -> Application: Rejoindre la discussion
group Débat
Utilisateur -> Application: Voter sa position dans le débat
Application -> Application: Enregistrement du vote de l'utilisateur
    loop
        
        Modérateur -> Application: Animer le débat en fonction du niveau de la discussion
        Utilisateur -> Application: Argumenter sa position et citer ses sources
        
        alt l'Utilisateur enfreint le règlement
            Modérateur -> Application: Sanctionner ou avertir des utilisateurs s'ils enfreignent le règlement de Rétorik
            Application -> Utilisateur: Sanctionner l'utilisateur
        end
    end
Utilisateur -> Application: Voter sa position finale
Application -> Application: Enregistrement du vote final de l'utilisateur
end

Modérateur -> Application: Mettre fin à la discussion

@enduml

